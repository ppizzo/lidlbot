# LIDL bot

In the current implementation this simple Python program searches for a string in the LIDL catalog, published on line on their website.

The catalog is in gzipped XML format and the URL can be customized in the code by modifying the value of the `url` variable.

## Install

Apart from Python3, you just have to install the required dependencies globally or in a virtualenv:

```bash
pip3 install -r requirements.txt
```

## Usage

The usage is quite straightforward, just call the program with the search value as command line argument:

```bash
$ python3 lidlbot.py <item>
```

Unfortunately, the catalog contains only the URLs of the products pages, there's no textual description. Therefore you must know how the URLs is usually coded for your favourite product and search for an excerpt of the URL.

For example, the wonderful "Guinness 50cl can" is usually added in the Italian catalog as `birra-stout`, so the search command is:

```bash
$ python3 lidlbot.py birra-stout
https://www.lidl.it/p/birra-stout/p10018730
```

The program will output the product URL is found or nothing if it's not found.

## Roadmap

 - [ ] Docker version
 - [ ] Notifications
 - [ ] Automated bot
