import requests
import gzip
import sys
import xml.etree.ElementTree as ET
from io import BytesIO

# URL of the gzipped XML file
url = "https://www.lidl.it/p/export/IT/it/product_sitemap.xml.gz"

# Function to download and parse the gzipped XML file
def parse_catalog(url, target):
    try:
        # Download the gzipped file
        response = requests.get(url)
        response.raise_for_status()

        # Decode the gzipped content
        with gzip.GzipFile(fileobj=BytesIO(response.content)) as gz:

            # Parse the XML content
            tree = ET.parse(gz)
            root = tree.getroot()

            # Scan the subtree
            for child in root:
                if target in child[0].text:
                    print(child[0].text)
                    sys.exit(0);

    except Exception as e:
        print("An error occurred: ", e)
        sys.exit(2)

    sys.exit(1)

if __name__ == "__main__":

    # Command line parsing
    if len(sys.argv) != 2:
        print(f"Usage: python3 {sys.argv[0]} <value>")
        sys.exit(3)

    target = sys.argv[1]

    # Search the item in the catalog
    parse_catalog(url, target)
